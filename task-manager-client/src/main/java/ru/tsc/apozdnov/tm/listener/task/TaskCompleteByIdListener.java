package ru.tsc.apozdnov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getName() == #event.name")
    public void executeEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}