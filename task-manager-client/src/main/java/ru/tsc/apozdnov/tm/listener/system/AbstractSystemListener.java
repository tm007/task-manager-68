package ru.tsc.apozdnov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.listener.AbstractListener;
import ru.tsc.apozdnov.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}