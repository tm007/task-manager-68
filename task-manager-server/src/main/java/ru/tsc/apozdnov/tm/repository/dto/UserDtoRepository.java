package ru.tsc.apozdnov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDtoModel> {

    @Nullable
    UserDtoModel findFirstByLogin(@NotNull String login);

    @Nullable
    UserDtoModel findFirstByEmail(@NotNull String email);
}