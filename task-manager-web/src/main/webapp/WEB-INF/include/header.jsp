<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<h1><a href="/">Менеджер задач</a></h1>
<head>
    <title>${viewName}</title>
    <style>
        h1 {
            font-family: "Times New Roman";
        }

        td {
            padding: 5px;
            border: navy dashed 1px;
        }

        th {
            font-weight: 700;
            text-align: left;
            background: darkgoldenrod;
            border: black solid 1px;
        }

        ul.hr {
            margin: 0;
            padding: 4px;
        }

        ul.hr li {
            display: inline;
            margin-right: 5px;
            border: 1px solid #000;
            padding: 3px;
            background: darkgoldenrod;
        }


    </style>
</head>
<body>
<ul class="hr">
    <li><a href="/tasks">Задачи</a></li>
    <li><a href="/projects">Проекты</a></li>
</ul>
<hr>