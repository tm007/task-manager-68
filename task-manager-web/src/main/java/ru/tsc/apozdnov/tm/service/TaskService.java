package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.ITaskService;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Transactional
    public Task add(@NotNull final String name) {
        final Task task = new Task(name);
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public Task add(@NotNull final Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @NotNull
    @Transactional
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Nullable
    @Transactional
    public Task findById(@NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Task save(@NotNull final Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public void remove(@NotNull final Task task) {
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void remove(@NotNull final List<Task> tasks) {
        tasks.forEach(this::remove);
    }

    @Override
    @Transactional
    public boolean existsById(@NotNull final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public long count() {
        return taskRepository.count();
    }

}