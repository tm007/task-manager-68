package ru.tsc.apozdnov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @Transactional
    Task add(@NotNull String name);

    @Transactional
    Task add(@NotNull Task task);

    @Transactional
    void removeById(@NotNull String id);

    @Transactional
    @NotNull List<Task> findAll();

    @Transactional
    @Nullable Task findById(@NotNull String id);

    @Transactional
    Task save(@NotNull Task task);

    @Transactional
    void remove(@NotNull Task task);

    @Transactional
    void remove(@NotNull List<Task> tasks);

    @Transactional
    boolean existsById(@NotNull String id);

    @Transactional
    void clear();

    @Transactional
    long count();
}
