package ru.tsc.apozdnov.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.tsc.apozdnov.tm.api.IProjectEndpoint;
import ru.tsc.apozdnov.tm.api.ITaskEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@ComponentScan("ru.tsc.apozdnov.tm")
public class WebApplicationConfiguration implements WebMvcConfigurer, WebApplicationInitializer {

    @Bean
    public InternalResourceViewResolver resolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final IProjectEndpoint IProjectEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, IProjectEndpoint);
        endpoint.publish("/project");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(@NotNull final ITaskEndpoint ITaskEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, ITaskEndpoint);
        endpoint.publish("/task");
        return endpoint;
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext) {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamicCXF = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

}