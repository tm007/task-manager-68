package ru.tsc.apozdnov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.apozdnov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/task")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    Task save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody Task task
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody Task task
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody List<Task> tasks
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

}