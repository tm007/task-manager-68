package ru.tsc.apozdnov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Transactional
    Project add(@NotNull String name);

    @Transactional
    Project add(@NotNull Project project);

    @Transactional
    void removeById(@NotNull String id);

    @Transactional
    @NotNull List<Project> findAll();

    @Transactional
    @Nullable Project findById(@NotNull String id);

    @Transactional
    Project save(@NotNull Project project);

    @Transactional
    void remove(@NotNull Project project);

    @Transactional
    void remove(@NotNull List<Project> projects);

    @Transactional
    boolean existsById(@NotNull String id);

    @Transactional
    void clear();

    @Transactional
    long count();
}
