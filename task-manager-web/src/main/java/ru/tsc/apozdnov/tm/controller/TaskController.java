package ru.tsc.apozdnov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.service.ProjectService;
import ru.tsc.apozdnov.tm.service.TaskService;

import java.util.List;
import java.util.UUID;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @NotNull
    private List<Task> getTasks() {
        return taskService.findAll();
    }

    @GetMapping("/task/create")
    public String create() {
        taskService.add("Task - " + UUID.randomUUID());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") Task task, BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Task task = taskService.findById(id);
        return new ModelAndView("task-edit", "task", task);
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAll();
    }

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", getTasks());
    }

}