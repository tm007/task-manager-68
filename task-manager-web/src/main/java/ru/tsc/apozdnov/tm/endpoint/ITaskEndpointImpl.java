package ru.tsc.apozdnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.apozdnov.tm.api.ITaskEndpoint;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.tsc.apozdnov.tm.api.ITaskEndpoint")
public class ITaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(taskService.findAll());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.existsById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final Task task
    ) {
        return taskService.add(task);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final Task task
    ) {
        taskService.remove(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final List<Task> tasks
    ) {
        taskService.remove(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear();
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

}