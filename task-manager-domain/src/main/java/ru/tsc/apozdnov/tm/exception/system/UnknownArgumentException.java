package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not supported...");
    }

    public UnknownArgumentException(final String argument) {
        super("Error! Argument `" + argument + "` not supported...");
    }

}